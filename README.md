# This is repo includes all the dotfiles that I use in my everyday life

* vimrc
* tmux.conf
* gitconfig

## To automagically fetch into the the right place on debian and debian-like systems execute:
`./boot.sh`


This will install *vim*, *git*, *tmux*, *sudo* and *curl*. 
It will also add your username to the sudoers. Log out and log in again.
