#!/bin/bash -e

USER=$(whoami)

su -c "apt install vim git tmux sudo" root

wget "https://gitlab.com/vpenkoff/dotfiles/-/raw/master/tmux.conf" -O ~/.tmux.conf -nc
wget "https://gitlab.com/vpenkoff/dotfiles/-/raw/master/vimrc" -O ~/.vimrc -nc
wget "https://gitlab.com/vpenkoff/dotfiles/-/raw/master/gitconfig" -O ~/.gitconfig -nc


su -c "echo \"$USER ALL=(ALL:ALL) ALL\" | sudo tee /etc/sudoers.d/$USER" root
